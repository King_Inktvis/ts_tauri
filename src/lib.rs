use proc_macro2::Ident;
use quote::quote;
use syn::{parse_macro_input, FnArg, ItemFn, ReturnType, Type};


struct Function {
    fn_name: Ident,
    output: String,
    args: Vec<Arg>,
}

struct Arg {
    name: String,
    arg_type: Ident,
}

impl Function {
    fn ts_interface(&self) -> String {
        let mut data = format!("interface {}Args {{\n", self.fn_name);
        // data.push_str(&format!("    {}: {};\n", self.name, self.arg_type));
        self.args.iter().for_each(|a| {
            data.push_str(&format!("    {}: {};\n", a.name, a.arg_type));
        });
        data.push_str("}\n");
        data
    }

    fn imports(&self) -> String {
        let mut ret = "import { invoke } from '@tauri-apps/api';\n".to_string();
        for a in self.args.iter() {
            ret.push_str(&format!(
                "import {{ {0} }} from '../bindings/{0}';\n",
                a.arg_type
            ));
        }
        ret
    }

    fn ts_function(&self) -> String {
        let mut arg = "{".to_string();
        for a in self.args.iter() {
            arg.push_str(&format!("{},", a.name));
        }
        arg.push('}');
        let mut par = "".to_string();
        for a in self.args.iter() {
            par.push_str(&format!("{}: {},", a.name, a.arg_type));
        }
        let data = format!(
            "export async function {}({}): Promise<{}> {{
    return await invoke(\"{}\", {});
}}\n",
            self.fn_name,
            par,
            Self::ts_type(&self.output),
            self.fn_name,
            arg
        );
        data
    }

    fn ts_type(ty: &str) -> &'static str {
        match ty {
            "String" => "string",
            _ => "",
        }
    }
}

#[proc_macro_attribute]
pub fn show_streams(
    _attr: proc_macro::TokenStream,
    item: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let new_item = item;
    let input = parse_macro_input!(new_item as ItemFn);
    let fn_name = input.sig.ident.clone();
    let output_type = match input.sig.output.clone() {
        ReturnType::Default => {
            panic!()
        }
        ReturnType::Type(_, b) => match *b {
            Type::Path(p) => p.path.segments[0].ident.clone(),
            _ => panic!(),
        },
    };
    let mut func = Function {
        fn_name: fn_name.clone(),
        output: output_type.to_string(),
        args: vec![],
    };
    for a in input.sig.inputs.iter() {
        match a {
            FnArg::Receiver(_) => {
                panic!()
            }
            FnArg::Typed(t) => {
                let arg_name = match *t.pat.clone() {
                    syn::Pat::Ident(pat_ident) => pat_ident.ident.to_string(),
                    _ => {
                        panic!()
                    }
                };
                let type_name = match *t.ty.clone() {
                    Type::Path(type_path) => {
                        println!("{}", type_path.path.segments[0].ident);
                        type_path.path.segments[0].ident.clone()
                    }
                    _ => panic!(),
                };
                let new_arg = Arg {
                    name: arg_name,
                    arg_type: type_name,
                };
                func.args.push(new_arg);
            }
        }
    }
    let test_fn = format!("gen_fn_bindings_{}", &func.fn_name)
        .parse::<proc_macro2::TokenStream>()
        .unwrap();
    let import_gen = func
        .args
        .iter()
        .map(|a| {
            let ty = &a.arg_type;
            let ty_name = ty.to_string();
            quote! {
                if let Some(loc) = <#ty>::EXPORT_TO {
                    let (imp_loc, _) = loc.split_at(loc.len() - 3);
                    imports.push(format!("import {{ {} }} from '../{}';\n", #ty_name, imp_loc));
                }
            }
        })
        .collect::<Vec<_>>();
    let template = format!(
        "export async function {}(): Promise<{{}}>{{{{
    return await invoke(\"{}\");
\
    }}}}",
        &fn_name, &fn_name,
    );
    let fn_body = quote! {
        let fn_body = format!(#template, <#output_type>::name());
    };
    let output_file = format!("bindings/{fn_name}.ts");
    let tmp = quote! {
        #input

        #[cfg(test)]
        mod #test_fn {
        use super::*;
            #[test]
            fn generate() {
                use std::fs;
                use std::io::Write;
                let mut imports:Vec<String> = vec!["import { invoke } from '@tauri-apps/api';".to_string()];

                #( #import_gen );*
                let mut text = imports.join("\n");
                text.push('\n');
                #fn_body
                text.push_str(&fn_body);
                let mut file = std::fs::File::create(#output_file).unwrap();
                file.write_all(text.as_bytes()).unwrap();
            }
        }
    };
    tmp.into()
}
